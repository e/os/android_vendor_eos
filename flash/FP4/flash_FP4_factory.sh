#!/usr/bin/env bash

# Target device info
PRODUCT="Fairphone 4"
PRODUCT_ID="FP4"

# Target Flashing Images info
FLASH_AB_FW_IMGS="bluetooth devcfg dsp modem xbl tz hyp keymaster abl aop featenabler imagefv multiimgoem qupfw uefisecapp xbl_config core_nhlos"
FLASH_AB_IMGS="boot dtbo recovery vbmeta vbmeta_system"
FLASH_A_IMGS="super"
ERASE_IMGS="userdata metadata"

# Target flash process behavior
CLEAN_FLASH=true
VIRTUAL_AB=true

source factory.common

# Common flashing function
flash_factory
