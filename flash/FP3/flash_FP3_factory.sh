#!/usr/bin/env bash

# Target device info
PRODUCT="Fairphone 3"
PRODUCT_ID="FP3"

# Target Flashing Images info
FLASH_AB_FW_IMGS="modem sbl1 rpm tz devcfg dsp aboot mdtp lksecapp cmnlib cmnlib64 keymaster"
FLASH_AB_IMGS="boot dtbo system vbmeta vendor"
FLASH_A_IMGS="userdata"

# Target flash process behavior
CLEAN_FLASH=true
VIRTUAL_AB=true

source factory.common

# Common flashing function
flash_factory
