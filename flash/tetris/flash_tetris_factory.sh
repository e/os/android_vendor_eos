#!/usr/bin/env bash

# Target device info
PRODUCT="CMF Phone 1"
PRODUCT_ID="k6878v1_64"

# Target Flashing Images info
FLASH_AB_FW_IMGS="apusys ccu connsys_bt connsys_gnss connsys_wifi dpm gpueb gz lk logo mcf_ota mcupm modem pi_img preloader:preloader_raw.img scp spmfw sspm tee vcp"
FLASH_AB_IMGS="boot dtbo init_boot vbmeta vbmeta_system vbmeta_vendor vendor_boot"
FLASH_A_IMGS="super"
ERASE_IMGS="userdata metadata"

# Target flash process behavior
CLEAN_FLASH=true
VIRTUAL_AB=true

source factory.common

# Common flashing function
flash_factory
