#!/usr/bin/env bash

# Target device info
PRODUCT="SHIFTphone 8"
PRODUCT_ID="otter"

# Target Flashing Images info
FLASH_AB_FW_IMGS="bluetooth devcfg dsp modem xbl tz hyp keymaster abl aop featenabler imagefv multiimgoem qupfw qweslicstore uefisecapp xbl_config cpucp shrm"
FLASH_AB_IMGS="boot dtbo vendor_boot vbmeta vbmeta_system"
FLASH_A_IMGS="super"
ERASE_IMGS="userdata metadata"

# Target flash process behavior
CLEAN_FLASH=true
VIRTUAL_AB=true

source factory.common

# Common flashing function
flash_factory
