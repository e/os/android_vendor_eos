#!/usr/bin/env bash

# Target device info
PRODUCT="Pixel 8"
PRODUCT_ID="shiba"

# Target Flashing Images info
FLASH_AB_FW_IMGS="abl bl1 bl2 bl31 gcf gsa gsa_bl1 ldfw modem pbl tzsw"
FLASH_AB_IMGS="boot init_boot dtbo vendor_kernel_boot vendor_boot vbmeta_system vbmeta_vendor vbmeta"
FLASH_A_IMGS="super"
ERASE_IMGS="userdata metadata"

# Target flash process behavior
CLEAN_FLASH=true
FW_BL_RESTART=true
VIRTUAL_AB=true

source factory.common

# Common flashing function
flash_factory
