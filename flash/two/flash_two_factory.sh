#!/usr/bin/env bash

# Target device info
PRODUCT="Murena Two"
PRODUCT_ID="k71v1_64_bsp"

# Target Flashing Images info
FLASH_AB_FW_IMGS="cam_vpu1 cam_vpu2 cam_vpu3 gz lk md1img preloader:preloader_k71v1_64_bsp.bin scp spmfw sspm tee"
FLASH_A_FW_IMGS="logo:logo.bin"
FLASH_AB_IMGS="boot dtbo vbmeta vbmeta_system vbmeta_vendor"
FLASH_A_IMGS="super"
ERASE_IMGS="userdata"
FORMAT_IMGS="md_udc"
VIRTUAL_AB=true

# Target flash process behavior
CLEAN_FLASH=true

source factory.common

# Common flashing function
flash_factory
