#!/usr/bin/env bash

# Target device info
PRODUCT="Fairphone 5"
PRODUCT_ID="FP5"

# Target Flashing Images info
FLASH_AB_FW_IMGS="bluetooth devcfg dsp modem xbl tz hyp keymaster abl aop featenabler imagefv multiimgoem qupfw uefisecapp xbl_config cpucp shrm"
FLASH_AB_IMGS="boot dtbo vendor_boot vbmeta vbmeta_system"
FLASH_A_IMGS="super"
ERASE_IMGS="userdata metadata"

# Target flash process behavior
CLEAN_FLASH=true
VIRTUAL_AB=true

source factory.common

# Common flashing function
flash_factory
