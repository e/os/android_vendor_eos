#!/usr/bin/env bash

# Target device info
PRODUCT="OnePlus Nord"
PRODUCT_ID="lito"

# Target Flashing Images info
FLASH_AB_FW_IMGS="abl aop bluetooth devcfg dsp featenabler hyp imagefv keymaster logo modem qupfw storsec tz uefisecapp xbl_config xbl"
FLASH_AB_IMGS="boot dtbo recovery vbmeta vbmeta_system"
FLASH_A_IMGS="odm product system system_ext vendor"
ERASE_IMGS="userdata metadata"

# Target flash process behavior
CLEAN_FLASH=true
VIRTUAL_AB=false
USE_FASTBOOTD=true

# Source common functions
source ./factory.common || { echo "ERROR: Failed to source factory.common"; exit 1; }

# Common flashing function
flash_factory
