#!/usr/bin/env bash

# Target device info
PRODUCT="SHIFT6mq"
PRODUCT_ID="sdm845"

# Target Flashing Images info
FLASH_AB_FW_IMGS="ImageFv aop bluetooth cmnlib cmnlib64 devcfg dsp hyp keymaster modem qupfw storsec tz xbl xbl_config"
FLASH_AB_IMGS="boot dtbo recovery vbmeta vbmeta_system vbmeta_vendor"
FLASH_A_IMGS="super userdata"
ERASE_IMGS="metadata"

# Target flash process behavior
CLEAN_FLASH=true
VIRTUAL_AB=true

source factory.common

# Common flashing function
flash_factory
